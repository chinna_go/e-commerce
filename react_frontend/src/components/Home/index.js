//imported external dependencies
import Cookies from "js-cookie";
import { Redirect, Link } from "react-router-dom";
//imported internal dependencies
import Header from "../Header";
//imported CSS file for styling
import "./index.css";
import Carousel from '../Carousel'

//written a function to redirect from the home to login using JWT_TOKEN
const Home = () => {
  const jwtToken = Cookies.get("jwt_token");
  if (jwtToken === undefined) {
    return <Redirect to="/login" />;
  }
//returning the component to display the content to the UI
  return (
    <>
      <Header />
      <div className="carousel-div">
        {/* <Carousel /> */}
      </div>
      <div className="home-container">
        <div className="home-content">
          <h1 className="home-heading">Meating Your Expectations.</h1>
          <img
            src="http://localhost:3000/images/home/meat.png"
            alt="MEAT that get you noticed"
            className="home-mobile-img"
          />
          <p className="home-description">
            If your local market's meat selection leaves something to be
            desired, try online meat delivery for quality beef, pork, chicken
            and more
          </p>
          <Link to="/products">
            <button type="button" className="shop-now-button">
              Shop Now
            </button>
          </Link>
        </div>
        <img
          src="http://localhost:3000/images/home/meat.png"
          alt="Meat that get you noticed"
          className="home-desktop-img"
        />
      </div>
    </>
  );
};

//exporting home module
export default Home;
