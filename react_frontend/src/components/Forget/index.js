//external dependencies
import {Component} from 'react'
import Cookies from 'js-cookie'
import {Link, Redirect} from 'react-router-dom'


//iternal dependencies
import './index.css'


{/* in login form ,taking the username and newPassword as a string
  *and showing error if user does not fill the details and not following some conditions 
*/}

class Forgot extends Component {
  state = {
    username: '',
    newPassword: '',
    securityQuestion:"",
    showSubmitError: false,
    errorMsg: '',
  }

  onChangeUsername = event => {
    this.setState({username: event.target.value})
  }

  onChangenewPassword = event => {
    this.setState({newPassword: event.target.value})
  }
  onChangenewSecurity = event => {
    this.setState({securityQuestion: event.target.value})
  }

  rendernewPasswordField = () => {
    const {newPassword} = this.state
    return (
      <>
        <label className="input-label" htmlFor="Password">
          New Password
        </label>
        <input
          type="Password"
          id="Password"
          className="username-input-field"
          value={newPassword}
          onChange={this.onChangenewPassword}
          placeholder="New Password"
        />
      </>
    )
  }

//renders Username 
  renderUsernameField = () => {
    const {username} = this.state
    return (
      <>
        <label className="input-label" htmlFor="username">
          Username
        </label>
        <input
          type="text"
          id="username"
          className="username-input-field"
          value={username}
          onChange={this.onChangeUsername}
          placeholder="Username"
        />
      </>
    )
  }

  onSubmitSuccess = jwtToken => {
    const {history} = this.props
    Cookies.remove('jwt_token')
    history.replace('/login')
  }

  onSubmitFailure = data => {
    this.setState({showSubmitError: true, errorMsg:data.status_message})
  }

  submitForm = async(event)=>{
    event.preventDefault()
    console.log("hello")
    const jwtToken = Cookies.get('jwt_token')
    const{username,newPassword,securityQuestion} = this.state
    const forgotDetails = {
      username,
      new_password: newPassword,
      security_question: securityQuestion
    }
    const url = "http://localhost:3001/forgot/user"
    const option = {
      method:"PUT",
      body: JSON.stringify(forgotDetails),
      headers: {
        "Content-Type":"application/json",
        "Accept":"application/json",
        "client_key": JSON.stringify(jwtToken)
      }
    }
    const response = await fetch(url,option)
    const data = await response.json()
    console.log(data)
    if (data.status_code !== 200) {
      this.onSubmitFailure(data)
    } else {
      this.onSubmitSuccess(data.jwt_token)
    }
  }
  
//rendering Security Question for user
  renderSecurityQuestion = () => {
    const {securityQuestion} = this.state
    return (
      <>
        <label className="input-label" htmlFor="securityQuestion">
          Security Question
        </label>
        <input
          type="text"
          id="username"
          className="username-input-field"
          value={securityQuestion}
          onChange={this.onChangenewSecurity}
          placeholder="Security Question"
        />
      </>
    )
  }
  
 
//The state object is where you store property values that belongs to the component
//When the state object changes, the component re-renders.
//checking jwt token is already defined or not if it  does not defined redirect to previous path
  render() {
    const {showSubmitError, errorMsg} = this.state
    const jwtToken = Cookies.get('jwt_token')
    if (jwtToken !== undefined) {
      return <Redirect to="/" />
    }
    return (
      <div className="login-form-container">
        <img
          src="https://webstockreview.net/images/caveman-clipart-rock-26.png"
          className="login-website-logo-mobile-image"
          alt="website logo"
        />
        <img
          src="https://webstockreview.net/images/caveman-clipart-rock-26.png"
          className="login-image"
          alt="website login"
        />
        <form className="form-container" onSubmit={this.submitForm}>
          <img
            src="https://cdn1.iconfinder.com/data/icons/ancient-human-using-tool-and-equipment/482/prehistoric-001-512.png"
            className="login-website-logo-desktop-image"
            alt="website logo"
          />
          <div className="input-container">{this.renderUsernameField()}</div>
          <div className="input-container">{this.rendernewPasswordField()}</div>
          <div className="input-container">{this.renderSecurityQuestion()}</div>
          <Link to = "/login" className='link12'> 
            <button type='submit' className='login-button'>Continue</button> 
          </Link>
          {showSubmitError && <p className='error-message'>{errorMsg}</p>}
         
        </form>
        
      </div>
    )
  }
}

//exporting forgot module
export default Forgot
