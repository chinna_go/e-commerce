//imported external dependencies
import {Component} from 'react'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import Cookies from 'js-cookie'

//imported CSS file for styling
import './index.css'

//written a function for sighnup and used state for getting the user signup by using required fields
//written event method for getting details updated from the user  
class SignUp extends Component{
    state = {
        name:"",
        username:"",
        password:"",
        email:"",
        phone_no:"",
        re_enter_password:"",
        security_question:"",
        error_msg:"",
        is_signup:false
      }
  
      changeName = (event)=>{
        this.setState({name:event.target.value})
      }
  
      changeUserName=(event)=>{
        this.setState({username:event.target.value})
      }
  
      changePassword = (event)=>{
        this.setState({password:event.target.value})
      }
  
      changeReEnterPassword = (event)=>{
        this.setState({re_enter_password:event.target.value})
      }
  
      changeEmail = (event)=>{
        this.setState({email:event.target.value})
      }
  
      changePhoneNo = (event)=>{
        this.setState({phone_no:event.target.value})
      }
  
      changeSecurityQuestion = (event)=>{
        this.setState({security_question:event.target.value})
      }
  
      redirectToSignin = ()=>{
        const{history} = this.props
        history.replace("/login")
      }
  
      submitingSignup = async(event)=>{
        event.preventDefault()
        const url = "http://localhost:3001/register/user"
        const{name,username,password,re_enter_password,email,phone_no,security_question} = this.state
        const userDetails = {
          name,
          username,
          password,
          re_enter_password,
          email,
          phone_no,
          security_question
        }
        const option = {
          method:"POST",
          body: JSON.stringify(userDetails),
          headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json'
           }
          // headers:{
          //   "Content-Type" :"application/json",
          //   "Accept":"application/json",
          //   "Access-Control-Allow-Origin":"http://localhost:3000",
          //   "Access-Control-Allow-Credentials":"true"
          // }
        }
        
        const response = await fetch(url,option)
        const data = await response.json()
        if(data.status_code !== 200){
          this.setState({error_msg:data.status_message,is_signup:true})
        }else{
          this.redirectToSignin()
        }
      }
  
    render(){
        const{name,username,password,re_enter_password,email,phone_no,security_question,error_msg,is_signup} = this.state
        const jwtToken = Cookies.get('jwt_token')
        if (jwtToken !== undefined) {
          return <Redirect to="/" />
        }
        return(
        <div>
            <article>
        <div className="leftCard">
          <header>
            <img width={130} alt="mahesh" src="https://www.logolynx.com/images/logolynx/fb/fbbe466b9024f642910404ead3f5e4a5.png" />
          </header>
          <section className="main">
            <h4>E-commerce<br />
              Trending<br />
              2022
            </h4>
            <img alt='mahesh' src="https://th.bing.com/th/id/OIP.xAM5odE0cKz0LJkWFvDAMgHaLd?pid=ImgDet&rs=1" />
          </section>
          <footer>
            <div className="winnerCards" id="#dvMain">
              <img alt='mahesh' src="https://th.bing.com/th/id/OIP.Bd_S7673KbEZ186gazWtKAHaHa?pid=ImgDet&rs=1" />
              <p>$10/-<br />per Kg</p>
            </div>
            <div className="winnerCards" id="#dvMain">
              <img alt='mahesh' src="https://th.bing.com/th/id/R.c5f757ce850a6d75d5af50736872edeb?rik=2oOHsBqW0x52jQ&riu=http%3a%2f%2fclipart-library.com%2fimg%2f1972822.png&ehk=Atb8KTvNzYZ%2biOceTF0gBIArKDS6tNad8yT4sBBCKjQ%3d&risl=&pid=ImgRaw&r=0" />
              <p>$15/-<br />fish</p>
            </div>
            <div className="winnerCards" id="#dvMain">
              <img alt='mahesh' src="https://cdn4.vectorstock.com/i/1000x1000/28/28/cartoon-happy-crab-isolated-on-white-background-vector-21032828.jpg" />
              <p>$50/-<br />Crabs</p>
            </div>
          </footer>
        </div>
        <div className="right-card">
          <div className="card-frame">
            <h4>Register</h4>
            <p>or  
                <Link to="/login" className="link1">
                    <span> login to your account</span>
                </Link>
            </p>
            <form onSubmit={this.submitingSignup}>
              <input className='input1' type="text" placeholder="Username" value={username} onChange={this.changeUserName} />
              <input className='input1' type="text" placeholder="Name" value={name} onChange={this.changeName} />
              <input type="text" className='input1' placeholder="Email" value={email} onChange={this.changeEmail} />
              <input type="password" className='input1' placeholder="Password" value={password} onChange={this.changePassword} />
              <input type="password" className='input1' placeholder="Re Enter Password" value={re_enter_password} onChange={this.changeReEnterPassword} />
              <input type="text" placeholder="Phone No" className='input1' value={phone_no} onChange={this.changePhoneNo} />
              <input type="text" placeholder="Security Question" className='input1' value={security_question} onChange={this.changeSecurityQuestion} />
              {is_signup &&<p className="register-error">* {error_msg}</p>}
            <button className='button1' type='submit'>Continue</button>
            </form>
             <a href='/#'>By creating an account, I accept the <b>Terms &amp; Conditions</b></a>
          </div>
        </div>
      </article>
        </div>
        )
    }
}


//exporting signup module
export default SignUp