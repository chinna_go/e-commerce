//external dependencies
import { request, response } from "express";

//internal dependency
import { USER_LOGIN_DATA } from "../utils/mongo_db.js";

// deleting the login data

/**
 
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * JWT Token is used to share security information between two parties — a client and a server
 * checking if error is there or not using condition, if error is not there login data should deleted otherwise it shows an error
 */

const deleting_login_data = (request,response)  => {
    const user_jwt = request.body.user_jwt_token
    

        USER_LOGIN_DATA.findOneAndRemove({jwt_token:user_jwt},(err)=>{
      
            if(err){
      
                console.log(err)
      
            } else {
                response.send(JSON.stringify("deleted data"))
            }
      
        }) 
}

export {deleting_login_data}