//internal dependencies
import { validate_admin_login_data_for_forget_password, validate_user_login_data_for_forget_password } from "../utils/authentication_validation.js";

/**
 * @path /forget/user_type is the path by using this path we can validate the user data and update the user/admin password
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */

 const forgot_user_handler = (request, response) => {
    // login_data is the variable that contains the request body data
    const login_data = request.body;
    // client_key is the jwt token only logedin user are access the restaurent data
    const {client_key} = request.headers
    // initializing the count is 0 because we need to validate the request parameters
    var count = 0;
    // iteraing over the body data because we need to count the body params
    for (let item in login_data) {
      // updating the count
      count = count + 1;
    }
    // geting user_type from request params
    const user_type = request.params.user_type;
    /*
      we need only username and password for user login
              if any extra params are pass in the request it throws error */
    if (count !== 3) {
      response.send(JSON.stringify(
        {status_code:406,
        status_message:"Parameters count is not matches with database fields"
      }));
    }
    // if parameter count are valid execution goes through the else block
    else {
      if(user_type === "user"){
        validate_user_login_data_for_forget_password(login_data,response,client_key)
      }else{
        validate_admin_login_data_for_forget_password(login_data,response,client_key)
      }
    }
  }

  
//exporting forgot_user_handler
export{forgot_user_handler}