import { request, response } from "express";
import {
  validate_register_data,
  validate_user_register_database,
  validate_admin_register_database,
  validate_user_login_data,
  validate_admin_login_data,
  validate_user_login_data_for_forget_password,
  validate_admin_login_data_for_forget_password,
} from "../utils/authentication_validation.js";

  import { POSTS_DATA, PRIME_DATA, DETAILED_PRODUCT_DATA, CAROUSEL_IMAGES_DATA } from "../utils/mongo_db.js";
  
    /**
   * @path /register is the path by using this path we can post the user register data
   * @param request is the callback parameter that contains the data that passed from client(postman)
   * based on that data server respond
   * @param response is the callback parameter that sends the data based on the request data
   */
  
  
  const register_user_handler = (request, response) => {
      // register_data is the variable that contains the request body data
    const register_data = request.body;
    // geting user_type from request params
    const user_type = request.params.user_type;
    // validate_register_data function returns an object that contains the status_code and the status message
    let validation_result = validate_register_data(register_data);
    // if returned result doesn't contains 200 status code the post method returns the error response
    if (validation_result.status_code !== 200) {
      let { status_message } = validation_result;
      // sending the response to the client
      response.send(JSON.stringify(validation_result));
    }
    // if validate_register_data does contains 200 status code it goes to this block
    else {
      // if username already exists in the database it returns some error
      if (user_type === "user") {
        // calling the validate_user_register_database that are defined in the authentication_validation.js
        validate_user_register_database(register_data,response,validation_result)
      } else {
        // calling the validate_admin_register_database that are defined in the authentication_validation.js
        validate_admin_register_database(register_data,response,validation_result)
      }
    }
  };

/**
 * @path /login is the path by using this path we can post the user login data and validate it using the register data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */

const login_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iteraing over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  // geting user_type from request params
  const user_type = request.params.user_type;
  /*
        we need only username and password for user login
                if any extra params are pass in the request it throws error */
  if (user_type === "user") {
    validate_user_login_data(login_data, response);
  } else {
    validate_admin_login_data(login_data, response);
  }
};
/**
 * @path /forget/user_type is the path by using this path we can validate the user data and update the user/admin password
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */

const forgot_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  // client_key is the jwt token only logedin user are access the restaurent data
  const { client_key } = request.headers;
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iteraing over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  // geting user_type from request params
  const user_type = request.params.user_type;
  /*
      we need only username and password for user login
              if any extra params are pass in the request it throws error */
  if (count !== 3) {
    response.send("Please enter valid parameter count");
  }
  // if parameter count are valid execution goes through the else block
  else {
    if (user_type === "user") {
      validate_user_login_data_for_forget_password(
        login_data,
        response,
        client_key
      );
    } else {
      validate_admin_login_data_for_forget_password(
        login_data,
        response,
        client_key
      );
    }
  }
};

/**

 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * creating the product data 
 * and push the data in array
 */
const post_details = [];
const post_the_data = (request, response) => {
  const title = request.body.title;
  const image = request.body.image;
  const type = request.body.type;
  const price = request.body.price;
  const rating = request.body.rating;

  const post_data = {
    title: title,
    image: image,
    type: type,
    price: price,
    rating: rating,
  };

 //saving the data using save()function to the database   
  const posts_data = new POSTS_DATA(post_data);
  posts_data.save().then(() => response.send("data saved"));
  post_details.push(posts_data);
};



/**
 *method:post-creating the prime deatils data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * posting the product data 
 */
const prime_details = [];
const post_the_prime_details = (request, response) => {
  const title = request.body.title;
  const image = request.body.image;
  const type = request.body.type;
  const price = request.body.price;
  const rating = request.body.rating;

  const post_data = {
    title: title,
    image: image,
    type: type,
    price: price,
    rating: rating,
  };

//saving the data using save()function to the database  
  const posts_data = new PRIME_DATA(post_data);
  posts_data.save().then(() => response.send("data saved"));
  prime_details.push(posts_data);
};



/**
 *method:post-creating the product details
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * creating the product details 
 
 */

const detailed_product = [];
const post_the_detailed_product = (request, response) => {
  const product_id = request.body.product_id;
  const title = request.body.title;
  const image = request.body.image;
  const type = request.body.type;
  const price = request.body.price;
  const rating = request.body.rating;
  const reviews = request.body.reviews;
  const description = request.body.description;
  const availability = request.body.availability;
  const similar_products = request.body.similar_products;

  const post_data = {
    title: title,
    image: image,
    type: type,
    price: price,
    rating: rating,
    product_id: product_id,
    similar_products: similar_products,
    reviews: reviews,
    availability: availability,
    description: description,
  };
 
  //and have to save data using save() function to database and push the data into array
  const posts_data = new DETAILED_PRODUCT_DATA(post_data);
  posts_data.save().then(() => response.send("data saved"));
  detailed_product.push(posts_data);
};


/**
 * @request  images from the body
 * post the data to init
 */
const carousel_images = [];
const post_carousel_images = (request, response) => {
  const image = request.body.image;

  const post_data = {
  
    image: image
  
  };
  const posts_data = new CAROUSEL_IMAGES_DATA(post_data);
  posts_data.save().then(() => response.send("data saved"));
  carousel_images.push(posts_data);
};

export { post_details, prime_details, detailed_product, carousel_images };


//exporting path_handlers
export {
  register_user_handler,
  login_user_handler,
  forgot_user_handler,
  post_the_data,
  post_the_prime_details,
  post_the_detailed_product,
  post_carousel_images
};
