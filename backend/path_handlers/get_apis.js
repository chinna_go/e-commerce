//external dependencies
import { request, response } from "express";

//internal dependencies
import {
  POSTS_DATA,
  PRIME_DATA,
  DETAILED_PRODUCT_DATA,
  CAROUSEL_IMAGES_DATA
} from "../utils/mongo_db.js";
import { post_details, prime_details, detailed_product , carousel_images } from "./post_apis.js";

/**
 
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 *To returns the first element in the provided array that satisfies the provided testing function , for that using The find() method
 */


const get_the_postdata = (request,response) => {
  const data = new POSTS_DATA(post_details);

    POSTS_DATA.find({}, (error,data) => {
      if (error) {
        console.log(error)
      } else {
        // send the data to the response
        response.send(data)
      }
    })
}

 

/**
 *method:get-to get the red_meat items 
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 *To returns the first element in the provided array that satisfies the provided testing function , for that using The find() method
   and giving type to display only red_meat items
 *  
 */
const get_the_red_meat = (request, response) => {
  const data = new POSTS_DATA(post_details);

  POSTS_DATA.find({ type: "RED_MEAT" }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};



/**
 *method:get-to get the POULTRY items 
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 *To returns the first element in the provided array that satisfies the provided testing function , for that using The find() method
   and giving type to display only poultry items
 *  
 */
const get_the_poultry = (request, response) => {
  const data = new POSTS_DATA(post_details);

  POSTS_DATA.find({ type: "POULTRY" }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};



/**
 *method:get-to get the PORK items 
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 *To returns the first element in the provided array that satisfies the provided testing function , for that using The find() method
   and giving type to display only pork items
 *  
 */
const get_the_pork = (request, response) => {
  const data = new POSTS_DATA(post_details);

  POSTS_DATA.find({ type: "PORK" }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};



/**
 *method:get-to get the seeFood items 
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 *To returns the first element in the provided array that satisfies the provided testing function , for that using The find() method
   and giving type to display only seefood items
 *  
 */
const get_the_seefood = (request, response) => {
  const data = new POSTS_DATA(post_details);

  POSTS_DATA.find({ type: "SEA_FOOD" }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};



/**
 *method:get-to get the exclusie prime products
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 * find()method is used to returns the first element in the provided array to get the prime details
 * */
const get_the_prime_products = (request, response) => {
  const data = new PRIME_DATA(prime_details);

  PRIME_DATA.find({}, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};


/**
 *method:get-to get the product details
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 *to get the product details based on id
 * */
const get_the_detailed_products = (request, response) => {
  const { id } = request.params;
  const data = new DETAILED_PRODUCT_DATA(detailed_product);

  DETAILED_PRODUCT_DATA.find({ product_id: id }, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(data);
    }
  });
};

// getting the carousel images from database

const get_the_carousel_images = (request, response) => {
  const data = new CAROUSEL_IMAGES_DATA(carousel_images);

  CAROUSEL_IMAGES_DATA.find({}, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response
      response.send(JSON.stringify(data));
    }
  });
};

// exporting all handler_functions
export {
  get_the_postdata,
  get_the_red_meat,
  get_the_poultry,
  get_the_pork,
  get_the_seefood,
  get_the_prime_products,
  get_the_detailed_products,
  get_the_carousel_images
};