// importing mangoose from mangoose
import mongoose from "mongoose";
// import register_data_schema from schema.js file
import {
  REGISTER_SCHEMA,
  LOGIN_SCHEMA,
  POST_SCHEMA,
  DETAILED_PRODUCT,
  CAROUSEL_IMAGES
} from "./schema.js";
// connecting the db
mongoose.connect(
  "mongodb+srv://Mahesh:WmysRZlFWrNCoeUY@cluster0.9brlo.mongodb.net/e_commerce?retryWrites=true&w=majority"
);

/* creating a model for the user_register_data for creating a list and enter objects in it */
const USER_REGISTER_DATA = mongoose.model(
  "USER_REGISTER_DATA",
  REGISTER_SCHEMA
);

/* creating a model for the post_data for creating the data for product*/ 
const POSTS_DATA = mongoose.model("POSTS_DATA", POST_SCHEMA);

/* creating a model for the prime_data for creating the data for product*/ 
const PRIME_DATA = mongoose.model("PRIME_DATA", POST_SCHEMA);

/* creating a model for the user_login_data for storing the user login details */
const USER_LOGIN_DATA = mongoose.model("USER_LOGIN_DATA", LOGIN_SCHEMA);

/* creating a model for the user_login_data for storing the user login details */
const ADMIN_LOGIN_DATA = mongoose.model("ADMIN_LOGIN_DATA", LOGIN_SCHEMA);

/* creating a model for the admin_lregister_data for creating a list and enter objects in it */
const ADMIN_REGISTER_DATA = mongoose.model(
  "ADMIN_REGISTER_DATA",
  REGISTER_SCHEMA
);

/* creating a model for the detailed_product_data for creating a list and enter objects in it */


const DETAILED_PRODUCT_DATA = mongoose.model(
  "DETAILED_PRODUCT_DATA",
  DETAILED_PRODUCT
);

const CAROUSEL_IMAGES_DATA = mongoose.model(
  "CAROUSEL_IMAGES_DATA",
  CAROUSEL_IMAGES
);

 /****************************Register*************** */  
/**
 *inserting the register data into the database
 *@params register_data - to insert the data into database
 *all the user register details have to insert and save into the database 
 */ 
 
function inserting_register_data_into_database(register_data) {
 
  const user_data = new USER_REGISTER_DATA({
    name: register_data.name,
    username: register_data.username,
    password: register_data.password,
    email: register_data.email,
    phone_number: register_data.phone_no,
    security_question: register_data.security_question,
  });
  // saving the data
  user_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

/**
 *inserting the register data into the database
 *@params admin_register_data - to insert the admin data into database
 *all the admin register details have to insert and save into the database 
 */ 
function inserting_admin_register_data_into_database(admin_register_data) {
  // const USER_LOGIN_data_1 = mongoose.model('USER_LOGIN_data_1', USER_LOGINPAGE_SCHEMA ); // schema
  const admin_data = new ADMIN_REGISTER_DATA({
    name: admin_register_data.name,
    username: admin_register_data.username,
    password: admin_register_data.password,
    //re_enter_password: admin_register_data.re_enter_password,
    email: admin_register_data.email,
    phone_number: admin_register_data.phone_no,
    security_question: admin_register_data.security_question,
  });
  // saving the data
  admin_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}


/***********************LOGIN**********************/ 
/**
 *inserting the user login data into the database
 *@params login_data - to insert the user login_data into database
 *all the user login details have to insert and save into the database 
 */ 


function inserting_user_login_data_into_database(login_data) {
  var date = new Date();
  login_data.date = date;
  const user_login_data = new USER_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  // saving the data
  user_login_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

/**
 *inserting the admin login data into the database
 *@params login_data - to insert the admin  login_data into database
 *all the admin login details have to insert and save into the database 
 */ 

function inserting_admin_login_data_into_database(login_data) {
  var date = new Date();
  login_data.date = date;
  const user_login_data = new ADMIN_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  // saving the data
  user_login_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

/* exporting the inserting_register_data_into_database, 
        inserting_user_login_data_into_database, inserting_admin_register_data_into_database
         functions and USER_REGISTER_DATA, ADMIN_REGISTER_DATA instaces.
    */
export {
  inserting_register_data_into_database,
  USER_REGISTER_DATA,
  ADMIN_REGISTER_DATA,
  USER_LOGIN_DATA,
  ADMIN_LOGIN_DATA,
  POSTS_DATA,
  PRIME_DATA,
  DETAILED_PRODUCT_DATA,
  CAROUSEL_IMAGES_DATA,
  inserting_user_login_data_into_database,
  inserting_admin_register_data_into_database,
  inserting_admin_login_data_into_database,
};
