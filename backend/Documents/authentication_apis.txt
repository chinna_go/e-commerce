1 api
method :POST
path :/register/user_type
schema: {
    name: String,
    username: String, 
    password: String,
    re_enter_password: String,
    email: String,
    phone_number: Number,
    security_question: String,
}
params: { user_type: "admin/user/prime_user" } 
Description :this admin can insert,delete,update the data

2 api
method :POST
path :/login/user_type
params: { user_type: "admin/user/prime_user" }
schema: {
    username: String,
    password: String,
    jwt_token: String,
    logon_time: Date,
}
Description :validate the admin credentials from the admin database and returns jwt token.

3 api
path : /forget_password/user_type
method : PUT
params: { user_type: "admin/user/prime_user", email, so on and so forth }
Description :it can ?validate the user login or not and if user login it updates the password