/*
    only admin user have access to post the api's data
*/

api1:
path: "/products"
method: POST
schema: {
    id: string,
    title: string,
    image_url: string,
    type: string,
    price: string,
    rating: string
}
Description: when we are calling api with "/products" path we are POSTting the object with respected data

api2:
path : "/prime_deals"
method : POST
schema:{
    id: string,
    title: string,
    image_url: string,
    type: string,
    price: string,
    rating: string
}
Description: when we are calling api with "/prime_deals" path we are POSTting the object with respected data

api3:
path : "/products/product_id"
method : POST
schema:{
    id: string,
    title: string,
    image_url: string,
    type: string,
    price: string,
    rating: string,
    similar_products: Array
}
Description: when we are calling api with "/products/product_id" path we are POSTting the object with respected data